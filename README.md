# Fallen Peak

An open source story brought to life through a community of creative minds

## Introduction

Fallen Peak is a fictitious place loosely based on the Peak District in the UK.  Our story is a fantasy adventure set in the near future and of course is about good vs evil, or is that social justice vs capitalist greed...  Our protagonists are the rebels who fight to restore peace to the land, who want nature to return to its former glory of legend and who are strive for equality and harmony for all. 

In Fallen Peak the constant demands of consumerism and the endless consumption of frivolous entertainment belong to a time well forgotten.  Tales of how people once squandered nature's resources to to live lavish, meaningless and synthetic lifestyles are told to our children as ghoulish fireside stories. But the ghosts of those tragic times haunt us all and we live with the consequences of the actions of our foolish ancestors everyday. 

However, in Fallen Peak corruption also endures the lessons of the history books and the same greed of those ancestors lives on in those who are in power over the land and the people.  Their efforts to control are uncaring and unthoughtful and cause great pain and suffering to many.  

Within Fallen peak is Casteltown, more of a village than a town with small cottages and shops tucked away in the sides of a steep sided valley.  Here is where Pip and Pop have their shop, Maker Do and Mend, their expertise in repairing and maintenance of just about anything.  They invent and create useful things from whatever junk they may have salvaged.  Their aim is to help their community do the most with what little they have.

Pip an Pop share a secret, it's a lab.  Their shop is mainly a front for the work they do that the people in power must never find out about.  This lab is the main hope of the people to restore balance, for them to regain control of their own land and destiny and to bring health back to nature.

### License

[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC non-commercial share alike](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png "Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License")
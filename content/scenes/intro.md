---
title: "Intro"
date: 2019-04-27T16:46:44+01:00
draft: true
---

The intro scene 

1. A commet is flying through space heading towards a tiny planet

1. From the ground/earth there are people who see the comet as a shooting
star.  It is nighttime and the people are doing various things:

1. A man and a girl who are have been star gazing from their home.  The
man says to the girl "you can make a wish if you want to"

1. Some runes are glowing and a voice asks "what does it mean?"

1. Some well dressed people are having a cocktail party in some high rise
city building, the banter between is laughing about how and powerful
they are (need to think about what they are saying exactly).  A
shooting star is flying past behind them but none of them notice.

1. A horse and cart is rattling down a dirty street with shacks either
side.  A large dark figure is hunched over, wearing a top hat and big
black coat.  With a whip in his hand he is geering at the horse to
pull harder.  The cart is runged with metal bars and the light from a
passing shooting star lights up some small scared faces of ragabond
children inside. 

1. A pair of mysterious eyes are reflected in a crystal ball, the
shooting star is flying through the crystal ball.  A voice says "They
say that shooting star bring good luck..." a cheshire cat type grin
croacks the words "let's see how much luck this _realy_ brings"






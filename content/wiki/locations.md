---
title: "Locations"
date: 2019-04-13T13:39:49+01:00
draft: true
---
## Castle Town

Nestled in a the Valley of Hope is a small town with an ancient
history.  This town, thousands of years old, has been inhabited since
people lived in the caves that pepper the hills that line the valley.
On one side of the town stands an ancient castle from which the the
town gets its name.  No longer in good repair the castle remains have
become overgrown and is used only by the local children when playing
their games.

Despite the heavy activity from the mines and factories throughout the
region Castle Town goes mostly unnoticed and The Corporation generally
overlook the town which they assume to be a slum with little worth.
However there is a community here that have not yet totally succumbed
to the toxins and famine spreading across the land.  The people here
maintain a self-contained ecosystem that has sustained a way of life
for as long as anyone can remember.

The people of Castle Town are peaceful and quiet, not wanting
attention from neighbouring threats.  They are poor but through
working together they create the best life possible in these dark
times.

## The Mill

Producing anything and everything that The Corporation demand The Mill
is a factory complex full of noise, dirt and stench.  Massive machines
churn away through night and day melding plastics and other synthetic
materials together into all sorts of products from cheap toys for the
wealthy to weapons for the Corporation's militia.  The scale of
production at The Mill requites a lot of energy to keep it going and
it's this that is a large contributing factor to the constant drive by
The Corporation to source more and more of the world's precious
natural resources.

Powering The Mill is just one aspect of keeping things running, it
takes workers too.  The Corporation doesn't like to pay for things
when it doesn't need to and so The Mill is operated largely by
so-called convicts from the slums, people sentenced to life
enslavement for crimes they either didn't do or for misdemeanours they
committed through their struggle to survive.  Living conditions are
very poor here, very few people are released and a sentence here is
almost certainly a sentence to death.

## Nine Ladies ##

Standing in a clearing of a woodland perched high on the plateaus of
Stanton Moor are 9 stones arranged in a circle.

## Solomon's Temple ##

Strange bowl shaped earth works and domed hillocks surround a stone
tower on the edge of the spa town of Buxton.

## Buxton Spa ##


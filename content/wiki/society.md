---
title: "Society"
date: 2019-04-14T16:24:05+01:00
draft: true
---
## Overview

Society in Fallen Peak has become somewhat simplified compared to the
complexities that had developed in the run up to the 22nd Century[^1].
Of course there are certain nuances and detail that are complex and
life is still very multi-faceted but society is now made up of just 
6, possibly 7 key groups - the haves and have-nots, the power and the
power-less, the ruthless and the just and the others who lurk out in
the wilds.

## The Corporation

They claim to serve society but really they control it, there is no
longer any need for a government and politicians merged into the
capitalist directorship of The Corporation long ago.

## The Slums

On the outskirts of the towns and cities people struggle to survive on
the waste and sometimes charity of the rich.  There is no social
security provided by The Corporation and the people here are
only considered by them to be an easy source of new workers for The
Mill and the mines.

The people in the slums are quite sickly, they live in toxic wastes,
scavenge for scraps of food and eat rats and other things with meagre
nutritional value.  Mutations due to consumed toxins and poor living
conditions mean that many people are born with physical differences.
These are largely unrecorded in medical journals and The Corporation
keep the media well away from broadcasting any of this to the Rich
people they serve.

## The Rich

Society has become truly divided, the people who have don't even
recognise there are other people any more.  These people have all
become the same, their outlook on life driven by their desire for
consumer goods and synthetic entertainments.  They have money but they
also lead very restricted and narrowed lives controlled by the
influences of The Corporation.

The Rich live in central cities and towns and rarely venture out other
than going on visiting other cities or entertainment complexes.  They
live in protected zones with the Militia guarding the perimeters.  The
air is purified, circulated and contained within large domes that also
act to protect from powerful solar radiation and airborne toxins.

## The New Settlers (aka Outlaws)

Throughout the land there are pockets of nature that is slowly
regenerating - Fallen Peak for example is one of these areas.  Hidden
in the valleys and hillsides of these areas small villages can be
found.  They are inhabited by the New Settlers, people who have been
bold enough to continue to live in the wilds away from the cities and
towns.  Some of these settlements are ancient whilst others are only a
generation old.

The people out here call themselves the New Settlers as they believe
they have a better chance at a new future away from the control of the
Corporation.  Some of these people have been here for centuries, some
have come from the inner Cities and some from the Slums.  They all
believe that they can work together to help nature return to good
health and create a new society based on equality and social justice.

The Corporation considers the New Settlers to be outlaws and call them
such.  This is why the New Settlers keep a very low profile and work
on very small scale projects.  The Rich are lead to believe that
people who live out in the wilds don't live for long, The Corporation
maintains the propaganda in fear that more people could one day alter
their values in the same way as the New Settlers.

## The Militia

## The Rebellion

## The Unknowables

[^1]: Due to the inordinate number of freeze dried noodle flavour options the first quarter of the 21st C saw the largest diversity in self-organising groups ever recorded. 

---
title: "Characters"
date: 2019-04-03T23:09:07+01:00
draft: false
---

## Pip
* **Age**: 13
* **Place**: Maker Do And Mend shop, Castle towne
* **Familiar**: A female Night Wolf called Eclipse.
* **Hobby**: Making, drawing, inventing, studying.
* **Family**: Pop is her farther and only known relative
* **Motivation**: Make the world a better place than you found it
* **Miscellaneous**: Pip is known for being very intelligent. She is quite a mysterious girl.

## Eclipse
* **Age**: 2
* **Place**: Maker Do And Mend shop, Castle town
* **Hobby**: laughing
* **Family**: fox family
* **Motivation**: 
* **Miscellaneous**: 

## Pop
* **Age**: 40
* **Place**: Maker Do And Mend shop, Castle town
* **Familiar**: A badger called Tali who is a master of disguise
* **Hobby**: Reading runes, exploring, gardening.
* **Family**: Pip is his daughter and they are the only known remaining survivors of their family.  Tali and Eclipse as their adopted family.
* **Motivation**: To protect Pip. To help others and make the most of the limited resources available. To get revenge on the tyranny that led to the destruction of their home town and surrounding lands.
* **Miscellaneous**: Pop is often found testing out new inventions, sometimes on himself, possibly when they're not really safe enough to test.  He is able to read runes which was taught to him once.  He has got a terrible memory, partly from an accident he's suffered and partly from having too many ideas and not staying focused on just one.

## Tali 
* **Age**: 27
* **Place**: Under the counter, Maker Do And Mend shop, Castle town
* **Hobby**: Cooking and Eating, Learning other languages, discovering shape shifting magic
* **Family**: Tali's one of 5 siblings: 2 sisters and 3 brothers his parents and their parents are still alive too.  In fact Tali has quite  a large family who get together a couple of times a year for their badger bash which includes drinking, music, games, fighting, and saucy dancing.
* **Motivation**: Food and beer and female badgers.  Tali would also like to have a family one day of his own but he's not quite ready to settle down.
* **Miscellaneous**:  Tali[^2] is extremely good at disguise, he likes magic that helps him look and sound like other creatures (or objects) though inside he's always a badger.

## The Egg Timer

* **Age**: 12,452 and a half
* **Place**: Previous last domain was TRes-2b[^1]
* **Hobby**: Planetary disintegration
* **Family**: GetMega-365-quantum-cluster
* **Motivation**: A ```do-while-true``` loop

## Mister Foozle (aka The Toad)

  * **Age**: 57
  * **Place**: The Mill
  * **Hobby**: Breeding big headed dogs
  * **Family**: Mrs Foozle 
  * **Motivation**: Money


## Lady Freyja

## The Sheep

[^1]: [TRes-2b](https://en.wikipedia.org/wiki/TrES-2b "Read about TRes-2b on Wikipedia") is the darkest planet ever found reflecting less than 1% of sunlight.  Nobody knows why this planet is darker than coal though we are sure The Egg Timer has some ideas.

[^2]: In Celtic mythology Cerridwen, an enchantress, gives birth to [Taliesin](https://en.wikipedia.org/wiki/Taliesin "Read about Taliesin on Wikipedia") who was previously known as Gwion.  Gwion was Cerridwen's servant, who became enlightened when accidentally tasting one of her potions.  Out of fear Gwion of being enslaved Gwion turned himself into various creatures to avoid Cerridwen catching him. However, when Gwion turned himself into a piece of grain Cerridwen ended up eating him and he was reborn as Taliesin.
